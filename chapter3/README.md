# Jogo Shoushiling

Mais conhecido como Pedra, Papel e Tesoura

``` mermaid
graph TD;

id1((Começo)) --> id2[Computador faz a sua escolha];

id2 --> id3[Obtém a escolha do usuário];
id3 --> id4{Examina as escolhas};
id4 --> |inválida| id3;
id4 --> |igual| id5[Determina empate];
id4 --> |diferente| id6[Determina o vencedor pelas regras];
id6 --> id7[Exibe o vencedor];
id5 --> id7

id7 --> id8((Fim de jogo))
```
