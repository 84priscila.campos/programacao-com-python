``` mermaid
graph TD;
id1((Começo)) --> id2[Armazenar lista de scores]
id2 --> id3[Listar amostras e scores]
id3 --> id4[Processar melhor score]
id4 --> id5[Processar Amostras com melhor score]
id4 --> id[Exibir score mais alto]
id5 --> id6[Exibir lista de amostras que contém o score mais alto]
```