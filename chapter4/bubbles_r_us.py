
# -*- coding: utf-8 -*-

scores = [60, 50, 60, 58, 54, 54, 58, 50, 52, 54, 
    48, 69, 34, 55, 51, 52, 44, 51, 69, 64, 66, 55, 52, 
    61, 46, 31, 57, 52, 44, 18, 41, 53, 55, 61, 51, 44]

length_scores = len(scores)
high_score = 0
list_high_score = []

for i in range(length_scores):
    index_string = str(i) 
    print(index_string, 'teve o score igual a:', scores[i])

    if (scores[i] > high_score):
        high_score = scores[i]

for i in range(length_scores):
    if (scores[i] == high_score):
        list_high_score.append(i)

print('')
print('Número Total de scores é: ', str(length_scores))
print('O score mais alto é: ', str(high_score))
print('Lista de scores mais alto: ', list_high_score)