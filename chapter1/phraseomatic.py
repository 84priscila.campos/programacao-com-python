import random

verbs = ['comer', 'andar', 'correr', 'ouvir', 'sorrir']
adjectives = ['bom', 'gostoso', 'melado', 'carinhoso']
nouns = ['hambúrguer', 'tênis', 'música', 'aparelho']

verb = random.choice(verbs)
adjective = random.choice(adjectives)
noun = random.choice(nouns)

phrase = verb + ' ' + adjective + ' ' + noun + '.'
print(phrase)